package com.airasia.service;

import org.apache.http.Header;
import org.json.JSONObject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings.Secure;

import com.airasia.holder.ConstantHolder;
import com.airasia.holder.PrefsHolder;
import com.airasia.mobile.MainActivity;
import com.airasia.util.CustomHttpClient;
import com.airasia.util.JSonHelper;
import com.airasia.util.LogHelper;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class SessionService  extends Service {

	Handler handler = new Handler();
	Handler timeHandler = new Handler();
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		LogHelper.debug("Session Service Start");
		handler.postDelayed(runabble, 500);
		return Service.START_NOT_STICKY;
//		return super.onStartCommand(intent, flags, startId);
	}
	
	

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}


	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}



	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	Runnable runabble = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			connUpdateSession();
		}
	};


	private void connUpdateSession(){
		RequestParams params = new RequestParams();
		params.put("request_from", ""+ConstantHolder.SESSION_TYPE);
		params.put("request_type", ""+ConstantHolder.LOGIN_STATUS);
		if(ConstantHolder.LOGIN_STATUS != 1){
			SharedPreferences prefs = getApplicationContext()
					.getSharedPreferences(PrefsHolder.PREFS, Context.MODE_PRIVATE);
			params.put("userSession", prefs.getString(PrefsHolder.SESSION_TICKET, ""));
		}
		
		CustomHttpClient.postWithHash(CustomHttpClient.GET_SESSION, params, new AsyncHttpResponseHandler() {
			
			
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				super.onFinish();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response = new String(responseBody);
				LogHelper.debug("Session Respose = " + response);
				int responseCode =  JSonHelper.GetResponseStatus(response);
				try {
					if(responseCode == JSonHelper.SUCCESS_REPONSE){
						JSONObject jsonResponse = new JSONObject(response);
						JSONObject data = jsonResponse.getJSONObject("data");
						
						String userSession = "";
						if (ConstantHolder.LOGIN_STATUS != 3){
							userSession= data.getString("userSession");
							LogHelper.debug("Session = " + userSession);
							MainActivity.act.timerSession();
						}else{
							userSession = "";
						}
						
						SharedPreferences prefs = getApplicationContext()
													.getSharedPreferences(PrefsHolder.PREFS, Context.MODE_PRIVATE);
						prefs.edit().putString(PrefsHolder.SESSION_TICKET, userSession).commit();
						if (ConstantHolder.LOGIN_STATUS == 1){
							ConstantHolder.LOGIN_STATUS = 2;
						}else if (ConstantHolder.LOGIN_STATUS == 3){
							ConstantHolder.LOGIN_STATUS = 1;
						}
						LogHelper.debug("Update status - > " + ConstantHolder.LOGIN_STATUS);
					}else{
						MainActivity.act.removeTimerSession();
					}
				}catch (Exception e){
					MainActivity.act.removeTimerSession();
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				MainActivity.act.removeTimerSession();
			}
		});
	}

}