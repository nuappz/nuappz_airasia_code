package com.airasia.fragment;

import java.util.List;

import org.apache.http.Header;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.LinearLayout;

import com.airasia.adapter.FrenFamilyAdapter;
import com.airasia.holder.PrefsHolder;
import com.airasia.mobile.MainActivity;
import com.airasia.mobile.R;
import com.airasia.model.MemberInfoModel;
import com.airasia.util.CustomHttpClient;
import com.airasia.util.JSonHelper;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class FrenFamilyFragment extends Fragment{

	AQuery aq;
	SharedPreferences prefs;
	int fromAct = 1;
	
	public static final int VIEW_FREN = 1;
	public static final int SELECT_FREN = 2;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	 @Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		
		LinearLayout view;

		prefs = getActivity().getSharedPreferences(PrefsHolder.PREFS, Context.MODE_PRIVATE);
		 
	 	view = (LinearLayout) inflater.inflate(R.layout.fragment_fren_family, null);
		 
		aq = new AQuery(view);
		
		connGetFrenFamily();
		 
		return view;
	}
	 
	
	public void connGetFrenFamily(){
		RequestParams params = new RequestParams();
		params.put("username", prefs.getString(PrefsHolder.USER_USERNAME, ""));
		params.put("ssoTicket", prefs.getString(PrefsHolder.SSO_TICKET, ""));
		
		
		CustomHttpClient.postWithHash(CustomHttpClient.GET_FREN_FAMILY, params, new AsyncHttpResponseHandler() {
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response = new String(responseBody);
				LogHelper.debug("Fren & family " +response);
				int responseCode = JSonHelper.GetResponseStatus(response);
				
				if(responseCode == JSonHelper.SUCCESS_REPONSE){
					List<MemberInfoModel> model = JSonHelper.parseFrenFamily(response);
					
					FrenFamilyAdapter adapter
						= new FrenFamilyAdapter(aq.id(R.id.expandable_list).getExpandableListView(),
										getActivity(), model);
					aq.id(R.id.expandable_list).getExpandableListView().setAdapter(adapter);
					setupExpandableList();
				}else{
					String errorMsg = JSonHelper.GetResponseMessage(response);
					MainActivity.showErrorMessage(getActivity(), errorMsg);
				}
				
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	void setupExpandableList(){
		
		aq.id(R.id.expandable_list).getExpandableListView().setOnGroupClickListener(new OnGroupClickListener() {
			int positionSelect = -1;
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// TODO Auto-generated method stub
				FrenFamilyAdapter adapter = (FrenFamilyAdapter) aq.id(R.id.expandable_list).getExpandableListView().getExpandableListAdapter();
	            
				if (fromAct == 1){
					if(groupPosition != positionSelect){
			            aq.id(R.id.expandable_list).getExpandableListView().collapseGroup(positionSelect);
			            adapter.positionExpand = groupPosition;
			            positionSelect = groupPosition;
					}else{
						adapter.positionExpand = -1;
						positionSelect = -1;
					}
					adapter.notifyDataSetChanged();
				}else{
					
				}
				return false;
			}
		});
	}
}