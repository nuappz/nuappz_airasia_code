package com.airasia.fragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.airasia.holder.PrefsHolder;
import com.airasia.mobile.MainActivity;
import com.airasia.mobile.R;
import com.airasia.model.PassportModel;
import com.airasia.util.ConstantHelper;
import com.airasia.util.CustomHttpClient;
import com.airasia.util.JSonHelper;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class MyProfileFragment extends Fragment{

	AQuery aq;
	SharedPreferences prefs;
	
	public static final int MY_PROFILE = 1;
	public static final int MY_DOCUMENT = 2;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	 public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		
		 LinearLayout view;

		 prefs = getActivity().getSharedPreferences(PrefsHolder.PREFS, Context.MODE_PRIVATE);
		 
		 int type = getArguments().getInt("type");
		 if(type == MY_PROFILE){
		 	view = (LinearLayout) inflater.inflate(R.layout.fragment_my_profile, null);
		 }else{
		 	view = (LinearLayout) inflater.inflate(R.layout.fragment_my_document, null);
		 }
		 
		 aq = new AQuery(view);
		 if (type == MY_PROFILE){
			 initMyProfile();
		 }else{
			 initTravelDocument();
		 }
		 
		 return view;
	}
	 
	private void initMyProfile(){
		
		String fname = prefs.getString(PrefsHolder.USER_FIRSTNAME, "");
		String lname = prefs.getString(PrefsHolder.USER_LASTNAME, "");
		String email = prefs.getString(PrefsHolder.USER_USERNAME, "");
		String national = prefs.getString(PrefsHolder.USER_NATIONALITY, "");
		String mobile = prefs.getString(PrefsHolder.USER_MOBILE, "");
		
		aq.id(R.id.profile_email).getTextView().setText(email);
		aq.id(R.id.profile_dob).getTextView().setText("" + ConstantHelper.getDateFromddMMyyyy(prefs.getString(PrefsHolder.USER_DOB, "")));
		aq.id(R.id.profile_firstName).getTextView().setText(fname);
		aq.id(R.id.profile_lastName).getTextView().setText(lname);
		aq.id(R.id.profile_nationality).getTextView().setText(national);
		aq.id(R.id.profile_mobile).getTextView().setText(""+mobile);
	}
	
	private void initTravelDocument(){
		connGetTravelDocument();
	}
	
	void connGetTravelDocument(){
		RequestParams params = new RequestParams();
		params.put("customerId", ""+prefs.getInt(PrefsHolder.USER_CID, 0));
		params.put("username", prefs.getString(PrefsHolder.USER_USERNAME, ""));
		params.put("ssoTicket", prefs.getString(PrefsHolder.SSO_TICKET, ""));
		
		CustomHttpClient.postWithHashWithoutServer(CustomHttpClient.GET_TRAVEL_DOC , params, new AsyncHttpResponseHandler() {
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response = new String(responseBody);
				LogHelper.debug("Travel Doc 1 asdad " +  response );
				response = response.replaceAll("null", "\"\"");
				LogHelper.debug("Travel Doc 2 dasda " +  response );
				JSONObject json;
				LogHelper.debug(response);
				try {
					int responseCode = JSonHelper.GetResponseStatus(response);
					String responseMsg = JSonHelper.GetResponseMessage(response);
					if (responseCode == JSonHelper.SUCCESS_REPONSE){
						json = new JSONObject(response);
						JSONObject data = json.getJSONObject("data");
						JSONObject childJson = data.getJSONObject("result");
						PassportModel model = JSonHelper.parsePassport(childJson);
						
						
						if (model!=null){
							aq.id(R.id.profile_passport).getTextView().setText(model.getNumber());
							aq.id(R.id.profile_issuing).getTextView().setText(model.getCountryCode());
							

							SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
							Date date = format.parse(model.getDate());
							
							Calendar mCalender = Calendar.getInstance();
							mCalender.set(date.getYear(), date.getMonth(), date.getDate());
							String displayDate = date.getDate() + " " 
										+ mCalender.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())
										+ " " + date.getYear();
							
							aq.id(R.id.profile_expiry_date).getTextView().setText(displayDate);
						}
						
						
					}else{
						MainActivity.showErrorMessage(getActivity(), responseMsg);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e){
					e.printStackTrace();
				}
				
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
}
