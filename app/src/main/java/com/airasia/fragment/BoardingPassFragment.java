package com.airasia.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.airasia.adapter.FragmentViewPageAdapter;
import com.airasia.layout.SlidingTabLayout;
import com.airasia.mobile.R;

public class BoardingPassFragment extends Fragment{

	int type;
	
	ViewPager pager;
	FragmentViewPageAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[]={"Upcoming","Flown"};
    int Numboftabs =2;
    View v;
	public static BoardingPassFragment newInstance(int _type) {
		BoardingPassFragment f = new BoardingPassFragment();
		f.type = _type;
		return f;
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {	
		 v = inflater.inflate(R.layout.fragment_boarding_list, null);
		
	 adapter =  new FragmentViewPageAdapter(getActivity().getSupportFragmentManager(),Titles,Numboftabs);
		 
	        // Assigning ViewPager View and setting the adapter
	        pager = (ViewPager)v.findViewById(R.id.pager);
	        pager.setAdapter(adapter);
	 
	        // Assiging the Sliding Tab Layout View
	        tabs = (SlidingTabLayout)v.findViewById(R.id.tabs);
	        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
	 
	        // Setting Custom Color for the Scroll bar indicator of the Tab View
	        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
	            @Override
	            public int getIndicatorColor(int position) {
	                return getResources().getColor(R.color.actionbar_bg);
	            }				
	        });
	 
	        // Setting the ViewPager For the SlidingTabsLayout
	        tabs.setViewPager(pager);
	 
	        Toast.makeText(getActivity(),"Hi Sai baba",Toast.LENGTH_SHORT).show();
		return v;
	}
	
	
}
