package com.airasia.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.airasia.adapter.HomePromoAdapter;
import com.airasia.holder.ConstantHolder;
import com.airasia.layout.CustomViewPager;
import com.airasia.mobile.R;
import com.androidquery.AQuery;

public class HomeFragment extends Fragment {

	CustomViewPager pager;
	
	AQuery aq;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 AQuery aq = new AQuery(getActivity());
//		 connGetBanner();
	}

	 @Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		
		 RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.fragment_main, null);
		 
		 aq = new AQuery(layout);
		 pager = (CustomViewPager) layout.findViewById(R.id.home_pager);
		 HomePromoAdapter adapter = new HomePromoAdapter(pager, getActivity(), ConstantHolder.bannerList);
		 pager.setAdapter(adapter);
		 setupIndicator(adapter.getCount());
		 pager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				switch(arg0){
				case 1:
					aq.id(R.id.home_indicator_1).enabled(true);
					aq.id(R.id.home_indicator_2).enabled(false);
					aq.id(R.id.home_indicator_3).enabled(true);
					aq.id(R.id.home_indicator_4).enabled(true);
					aq.id(R.id.home_indicator_5).enabled(true);
					break;
				case 2:
					aq.id(R.id.home_indicator_1).enabled(true);
					aq.id(R.id.home_indicator_2).enabled(true);
					aq.id(R.id.home_indicator_3).enabled(false);
					aq.id(R.id.home_indicator_4).enabled(true);
					aq.id(R.id.home_indicator_5).enabled(true);
					break;
				case 3:
					aq.id(R.id.home_indicator_1).enabled(true);
					aq.id(R.id.home_indicator_2).enabled(true);
					aq.id(R.id.home_indicator_3).enabled(true);
					aq.id(R.id.home_indicator_4).enabled(false);
					aq.id(R.id.home_indicator_5).enabled(true);
					break;
				case 4:
					aq.id(R.id.home_indicator_1).enabled(true);
					aq.id(R.id.home_indicator_2).enabled(true);
					aq.id(R.id.home_indicator_3).enabled(true);
					aq.id(R.id.home_indicator_4).enabled(true);
					aq.id(R.id.home_indicator_5).enabled(false);
					break;
				default:
					aq.id(R.id.home_indicator_1).enabled(false);
					aq.id(R.id.home_indicator_2).enabled(true);
					aq.id(R.id.home_indicator_3).enabled(true);
					aq.id(R.id.home_indicator_4).enabled(true);
					aq.id(R.id.home_indicator_5).enabled(true);
					break;
				}
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});

//		 connGetBanner();
		 return layout;
	}
	 
	void setupIndicator(int size){

		aq.id(R.id.home_indicator_1).enabled(false);
		switch(size){
		case 1:
			aq.id(R.id.home_indicator_1).visible();
			aq.id(R.id.home_indicator_2).gone();
			aq.id(R.id.home_indicator_3).gone();
			aq.id(R.id.home_indicator_4).gone();
			aq.id(R.id.home_indicator_5).gone();
			break;
		case 2:
			aq.id(R.id.home_indicator_1).visible();
			aq.id(R.id.home_indicator_2).visible();
			aq.id(R.id.home_indicator_3).gone();
			aq.id(R.id.home_indicator_4).gone();
			aq.id(R.id.home_indicator_5).gone();
			break;
		case 3:
			aq.id(R.id.home_indicator_1).visible();
			aq.id(R.id.home_indicator_2).visible();
			aq.id(R.id.home_indicator_3).visible();
			aq.id(R.id.home_indicator_4).gone();
			aq.id(R.id.home_indicator_5).gone();
			break;
		case 4:
			aq.id(R.id.home_indicator_1).visible();
			aq.id(R.id.home_indicator_2).visible();
			aq.id(R.id.home_indicator_3).visible();
			aq.id(R.id.home_indicator_4).visible();
			aq.id(R.id.home_indicator_5).gone();
			break;
		default:
			aq.id(R.id.home_indicator_1).visible();
			aq.id(R.id.home_indicator_2).visible();
			aq.id(R.id.home_indicator_3).visible();
			aq.id(R.id.home_indicator_4).visible();
			aq.id(R.id.home_indicator_5).visible();
			break;
		}
	}
	
}

