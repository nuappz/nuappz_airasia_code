package com.airasia.fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.GridLayout;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;

import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.airasia.layout.GridLayoutView;
import com.airasia.layout.GridLayoutView.OnToggledListener;
import com.airasia.mobile.R;


/**
 * Created by NUappz1 on 7/26/2015.
 */
public class SeatMapSelectFragment extends
        Fragment {//implements OnToggledListener {

   /* GridLayoutView[] myViews;

    GridLayout myGridLayout;*/

    int status[][];
    int i=0;
    int j=0;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_seatmapselectpick, container, false);

        String[] column/*row*/ = { "A", "B", "C", "D","E","F","G","H","I"};
        String[] row/*column*/ = { "1", "2", "3", "4","5","6" };
        int rl=row.length+1; int cl=column.length+1;
        status = new int[rl][cl];

        ScrollView sv = (ScrollView)v.findViewById(R.id.scrollView_seatMap);
        TableLayout tableLayout = createTableLayout(row, column,rl, cl);
        sv.addView(tableLayout);

      /*  ScrollView sv = new ScrollView(getActivity());
        TableLayout tableLayout = createTableLayout(row, column,rl, cl);
//        HorizontalScrollView hsv = new HorizontalScrollView(this);

//        hsv.addView(tableLayout);
        sv.addView(tableLayout*//*hsv*//*);
        setContentView(sv);*/

        //myGridLayout = (GridLayout)v.findViewById(R.id.mygrid);

       /* int numOfCol = myGridLayout.getColumnCount();
        int numOfRow = myGridLayout.getRowCount();
        myViews = new GridLayoutView[numOfCol*numOfRow];
        for(int yPos=0; yPos<numOfRow; yPos++){
            for(int xPos=0; xPos<numOfCol; xPos++){
                GridLayoutView tView = new GridLayoutView(getActivity(), xPos, yPos);
                tView.setOnToggledListener(this);
                myViews[yPos*numOfCol + xPos] = tView;
                myGridLayout.addView(tView);
            }
        }*/

      /*  myGridLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                final int MARGIN = 5;

                int pWidth = myGridLayout.getWidth();
                int pHeight = myGridLayout.getHeight();
                int numOfCol = myGridLayout.getColumnCount();
                int numOfRow = myGridLayout.getRowCount();
                int w = pWidth / numOfCol;
                int h = pHeight / numOfRow;

                for (int yPos = 0; yPos < numOfRow; yPos++) {
                    for (int xPos = 0; xPos < numOfCol; xPos++) {
                        GridLayout.LayoutParams params =
                                (GridLayout.LayoutParams) myViews[yPos * numOfCol + xPos].getLayoutParams();
                        params.width = w - 2 * MARGIN;
                        params.height = h - 2 * MARGIN;
                        params.setMargins(MARGIN, MARGIN, MARGIN, MARGIN);
                        myViews[yPos * numOfCol + xPos].setLayoutParams(params);
                    }
                }


            }
        });
*/
        return v;
    }

   /* @Override
    public void OnToggled(GridLayoutView v, boolean touchOn) {

        //get the id string
        String idString = v.getIdX() + ":" + v.getIdY();

       *//* int i = v.getIdX();
        int j = v.getIdY();
        if (i == 0 || j == 0){
            v.setVisibility(View.GONE);
        }*//*



        Toast.makeText(getActivity(),
                "Toogled:\n" +
                        idString + "\n" +
                        touchOn,
                Toast.LENGTH_SHORT).show();
    }*/

    private TableLayout createTableLayout(String [] rv, String [] cv,int rowCount, int columnCount) {
        // 1) Create a tableLayout and its params
        TableLayout.LayoutParams tableLayoutParams = new TableLayout.LayoutParams();
        TableLayout tableLayout = new TableLayout(getActivity());
        tableLayout.setBackgroundColor(Color.BLACK);

        // 2) create tableRow params
        TableRow.LayoutParams tableRowParams = new TableRow.LayoutParams();


       /* tableLayout.setShrinkAllColumns(true);
        tableLayout.setStretchAllColumns(true);
        tableLayout.setPadding(10, 10, 10, 10);*/

        //tableRowParams.setMargins(1, 1, 1, 1);
        tableRowParams.weight = 1;

        for (/*int */i = 0; i < rowCount; i++) {
            // 3) create tableRow
            TableRow tableRow = new TableRow(getActivity());
            tableRow.setBackgroundColor(Color.WHITE);
            tableRow.setPadding(2,2,2,2);
            for (/*int */j= 0; j < columnCount; j++) {
                // 4) create textView
                final TextView textView = new TextView(getActivity());
                textView.setHeight(80);
                //  textView.setText(String.valueOf(j));
                textView.setBackgroundColor(Color.WHITE);
                textView.setGravity(Gravity.CENTER);

                String s1 = Integer.toString(i);
                String s2 = Integer.toString(j);
//       String s3 = cv[j-1]+rv[i-1];//s1 + s2;
//       int id = Integer.parseInt(s3);
//       Log.d("TAG", "-___>"+id);
                if (i ==0 && j==0){
//                  textView.setText("0==0");
                } else if(i==0){
                    textView.setText(cv[j-1]);
                }else if( j==0){
                    textView.setText(rv[i-1]);
                }else {
                    String s3 = cv[j-1]+rv[i-1];
                    textView.setText(""+s3);
                    status[i-1][j-1] = 0;
                    textView.setBackgroundColor(Color.LTGRAY);
                    textView.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            Toast.makeText(getActivity(), textView.getText().toString(), Toast.LENGTH_LONG).show();
                            if(status[i-1][j-1] == 0)
                            {
                                textView.setBackgroundColor(Color.WHITE);
                                status[i-1][j-1] = 1;
                            }
                            else if(status[i-1][j-1] == 1)
                            {
                                textView.setBackgroundColor(Color.LTGRAY);
                                status[i-1][j-1] = 0;
                            }
                        }
                    });
                    // check id=23
//                 if(id==23){
//                   textView.setText("ID=23");
//
//                  }
                }

                // 5) add textView to tableRow
                tableRow.addView(textView, tableRowParams);
            }

            // 6) add tableRow to tableLayout
            tableLayout.addView(tableRow, tableLayoutParams);
        }

        return tableLayout;
    }
}



