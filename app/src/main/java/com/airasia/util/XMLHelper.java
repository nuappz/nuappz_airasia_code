package com.airasia.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.content.Context;
import android.content.res.AssetManager;

public class XMLHelper {

	public static final String SOAP_RESULTCODE = "ResultCode";
	public static final String SOAP_RESULTMESSAGE = "ResultMessage";
	public static final String CUSTOMER_NUMBER = "a:CustomerNumber";
	public static final String FIRSTNAME = "a:FirstName";
	public static final String LASTNAME = "a:LastName";
	public static final String TICKET_ID = "a:TicketId";
	
	
	public static String getTextFile(String name, Context context) {

		String result = null;
		try {
			AssetManager am = context.getAssets();

			InputStream iS = am.open(name);
			// create a buffer that has the same size as the InputStream
			byte[] buffer = new byte[iS.available()];
			// read the text file as a stream, into the buffer
			iS.read(buffer);
			// create a output stream to write the buffer into
			ByteArrayOutputStream oS = new ByteArrayOutputStream();
			// write this buffer to the output stream
			oS.write(buffer);
			// Close the Input and Output streams
			result = oS.toString();
			oS.close();
			iS.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
		return result;

	}

	public static String getAttributeText(Node node, String tag) {
		String value = "";
		try {
			Node noteResult = node.getAttributes().getNamedItem(tag);
			if (noteResult == null)
				value = "";
			else
				value = noteResult.getNodeValue();

		} catch (Exception ex) {
			value = "";
		}

		return value;
	}

	public static int getAttributeInteger(Node node, String tag) {
		int value = 0;
		try {
			Node noteResult = node.getAttributes().getNamedItem(tag);
			if (noteResult == null)
				value = 0;
			else
				value = Integer.parseInt(noteResult.getNodeValue());

		} catch (Exception ex) {
			value = 0;
		}
		return value;
	}

	public static long getAttributeLong(Node node, String tag) {
		
		
		long value = 0;
		try {
			Node noteResult = node.getAttributes().getNamedItem(tag);
			if (noteResult == null)
				value = 0;
			else
				value = Long.parseLong(noteResult.getNodeValue());

		} catch (Exception ex) {
			value = 0;
		}
		return value;
	}
	
	public static String getValue(Element e , String Tag){
		if(e==null)
		{
			return "";
		}
		NodeList nodeList = e.getElementsByTagName(Tag);
		if (nodeList !=null){
			Node node = nodeList.item(0);
			if (node!=null){
				Node child = node.getFirstChild();
				while (child !=null){
					if (child.getNodeType() == Node.TEXT_NODE) {
	                    return child.getNodeValue();
	                }
	                child = child.getNextSibling();
				}
			}
		}
		return "";
	}
	
	public static int getValueInt(Element e , String Tag){
		
		if(e==null)
		{
			return 0;
		}
		NodeList nodeList = e.getElementsByTagName(Tag);
		if (nodeList !=null){
			Node node = nodeList.item(0);
			if (node!=null){
				Node child = node.getFirstChild();
				while (child !=null){
					if (child.getNodeType() == Node.TEXT_NODE) {
	                    return Integer.parseInt(child.getNodeValue());
	                }
	                child = child.getNextSibling();
				}
			}
		}
		return 0;
	}
	
	public static int getLoginResultCode(Element e , String Tag){
		
		if(e==null)
		{
			return -1;
		}
		NodeList nodeList = e.getElementsByTagName(Tag);
		if (nodeList !=null){
			Node node = nodeList.item(0);
			if (node!=null){
				Node child = node.getFirstChild();
				while (child !=null){
					if (child.getNodeType() == Node.TEXT_NODE) {
	                    return Integer.parseInt(child.getNodeValue());
	                }
	                child = child.getNextSibling();
				}
			}
		}
		return -1;
	}
	
	public static String[] parseMemberInfo(Element e){
		String[] memberInfo = new String[4];
		memberInfo[0] = getValue(e, CUSTOMER_NUMBER);
		if (memberInfo[0].length()<=0){
			memberInfo[0] = "0";
		}
		
		memberInfo[1] = getValue(e, FIRSTNAME);
		if (memberInfo[0].length()<=0){
			memberInfo[0] = "0";
		}
		
		memberInfo[2] = getValue(e, LASTNAME);
		if (memberInfo[0].length()<=0){
			memberInfo[0] = "0";
		}
		
		memberInfo[4] = getValue(e, TICKET_ID);
		if (memberInfo[0].length()<=0){
			memberInfo[0] = "0";
		}
		
		return memberInfo;
		
	}
}
