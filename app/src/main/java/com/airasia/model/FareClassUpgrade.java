package com.airasia.model;

import java.util.List;

public class FareClassUpgrade {

	int key=0;
	String title, upgrade_title;
	List<String> benefits;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUpgrade_title() {
		return upgrade_title;
	}
	public void setUpgrade_title(String upgrade_title) {
		this.upgrade_title = upgrade_title;
	}
	public List<String> getBenefits() {
		return benefits;
	}
	public void setBenefits(List<String> benefits) {
		this.benefits = benefits;
	}
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	
}
