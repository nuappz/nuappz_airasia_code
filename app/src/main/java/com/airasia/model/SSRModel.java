package com.airasia.model;

public class SSRModel {

	String ssrCode, ssrNumber , descriptio;
	
	int available;
	double amount;
	

	public String getDescriptio() {
		return descriptio;
	}

	public void setDescriptio(String descriptio) {
		this.descriptio = descriptio;
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getSsrNumber() {
		return ssrNumber;
	}

	public void setSsrNumber(String ssrNumber) {
		this.ssrNumber = ssrNumber;
	}

	public int getAvailable() {
		return available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
	
}
