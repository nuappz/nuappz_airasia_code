package com.airasia.model;

import java.util.List;

public class FlightsModel {

	String flightDate, fromCity, toCity,
		recordLocator, bookId, passengerId, bookStatus, fligthNum,
		channelType, organizationCode, domainCode, agentCode, expiredDate,
		systemCode, stopStation, STD, STA;
	boolean editable, allotModifyGDS;
	
	List<MemberInfoModel> members;

	public String getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	public String getFromCity() {
		return fromCity;
	}

	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}

	public String getToCity() {
		return toCity;
	}

	public void setToCity(String toCity) {
		this.toCity = toCity;
	}

	public String getRecordLocator() {
		return recordLocator;
	}

	public void setRecordLocator(String recordLocator) {
		this.recordLocator = recordLocator;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getPassengerId() {
		return passengerId;
	}

	public void setPassengerId(String passengerId) {
		this.passengerId = passengerId;
	}

	public String getBookStatus() {
		return bookStatus;
	}

	public void setBookStatus(String bookStatus) {
		this.bookStatus = bookStatus;
	}

	public String getFligthNum() {
		return fligthNum;
	}

	public void setFligthNum(String fligthNum) {
		this.fligthNum = fligthNum;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getDomainCode() {
		return domainCode;
	}

	public void setDomainCode(String domainCode) {
		this.domainCode = domainCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public String getStopStation() {
		return stopStation;
	}

	public void setStopStation(String stopStation) {
		this.stopStation = stopStation;
	}

	public String getSTD() {
		return STD;
	}

	public void setSTD(String sTD) {
		STD = sTD;
	}

	public String getSTA() {
		return STA;
	}

	public void setSTA(String sTA) {
		STA = sTA;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public boolean isAllotModifyGDS() {
		return allotModifyGDS;
	}

	public void setAllotModifyGDS(boolean allotModifyGDS) {
		this.allotModifyGDS = allotModifyGDS;
	}

	public List<MemberInfoModel> getMembers() {
		return members;
	}

	public void setMembers(List<MemberInfoModel> members) {
		this.members = members;
	}
	
	
	
	
}
