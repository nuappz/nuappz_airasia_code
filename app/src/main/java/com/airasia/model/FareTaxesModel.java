package com.airasia.model;

import java.io.Serializable;

public class FareTaxesModel implements Serializable {

	String key, title;
	double amount = 0;
	
	public FareTaxesModel(){
		this.key = "";
		this.title = "";
	}
	
	public FareTaxesModel(String _key, String _title){
		this.key = _key;
		this.title = _title;
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
	
}
