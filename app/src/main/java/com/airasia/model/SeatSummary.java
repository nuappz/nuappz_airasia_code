package com.airasia.model;

import java.io.Serializable;

public class SeatSummary implements Serializable {

	String pName, seatNum, currency, equipType, equipSuffix, PRBCode, typeOfSeat;
	
	float price;

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getSeatNum() {
		return seatNum;
	}

	public void setSeatNum(String seatNum) {
		this.seatNum = seatNum;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getEquipType() {
		return equipType;
	}

	public void setEquipType(String equipType) {
		this.equipType = equipType;
	}

	public String getEquipSuffix() {
		return equipSuffix;
	}

	public void setEquipSuffix(String equipSuffix) {
		this.equipSuffix = equipSuffix;
	}

	public String getPRBCode() {
		return PRBCode;
	}

	public void setPRBCode(String pRBCode) {
		PRBCode = pRBCode;
	}

	public String getTypeOfSeat() {
		return typeOfSeat;
	}

	public void setTypeOfSeat(String typeOfSeat) {
		this.typeOfSeat = typeOfSeat;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	public void setPrice(String price) {
		this.price = Float.parseFloat(price);
	}

	

	
}
