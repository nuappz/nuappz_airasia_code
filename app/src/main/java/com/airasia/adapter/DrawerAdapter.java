package com.airasia.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.airasia.mobile.R;
import com.airasia.model.DrawerModel;
import com.androidquery.AQuery;

public class DrawerAdapter extends BaseAdapter {

	List<DrawerModel> items;
	LayoutInflater inflater;
	ViewHolder holder;
	AQuery aq;
	Activity act;
	
	public DrawerAdapter(List<DrawerModel> _items, Activity _act, View view){
		this.items = _items;
		inflater = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aq = new AQuery(view);
		this.act = _act;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		if (position == 0){
			return 0;
		}else{
			return 1;
		}
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int pos, View view, ViewGroup arg2) {
		// TODO Auto-generated method stub
		View vi = view;
		if (vi == null){
			if (getItemViewType(pos)==1){
				vi = inflater.inflate(R.layout.drawer_layout, null);
				holder = new ViewHolder();
				holder.title = (TextView) vi.findViewById(R.id.drawer_text);
				holder.arrow = (ImageView) vi.findViewById(R.id.drawer_arrow);
				holder.divider = (View) vi.findViewById(R.id.drawer_divider);
			}else{
				vi = inflater.inflate(R.layout.drawer_banner, null);
			}
			vi.setTag(holder);
		}else{
			if (getItemViewType(pos)==1){
				holder = (ViewHolder) vi.getTag();
			}
		}
		AQuery taq = this.aq.recycle(vi);

		if (getItemViewType(pos)==1){
			taq.id(holder.title).text(items.get(pos).getName());
			if (pos==1){
				taq.id(holder.title).getTextView().setTextColor(act.getResources().getColor(R.color.airasia_theme_red));
				taq.id(holder.divider).visible();
				taq.id(holder.arrow).visible();
			}else{
				taq.id(holder.title).getTextView().setTextColor(act.getResources().getColor(R.color.black));
				taq.id(holder.divider).gone();
				taq.id(holder.arrow).gone();
			}
		}
		
		return vi;
	}
	
	class ViewHolder{
		TextView title;
		ImageView arrow;
		View divider;
	}
	

}
