package com.airasia.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airasia.holder.ConstantHolder;
import com.airasia.holder.ViewHolder;
import com.airasia.mobile.R;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.MemberInfoModel;
import com.airasia.model.SSRContainer;
import com.airasia.model.SSRItemModel;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MealListingAdapter extends BaseAdapter{

	SSRContainer container;
	
	LayoutInflater inflater;
	BookingInfoModel booking;
	ViewHolder.MealHolder holder;
	AQuery aq;
	MemberInfoModel person;
	Activity act;
	
	public MealListingAdapter(SSRContainer _container,
			Activity _act,
			View view,
			BookingInfoModel _booking,
			MemberInfoModel _person){
		this.person = _person;
		this.container = _container;
		inflater = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aq = new AQuery(view);
		this.act = _act;
		this.booking = _booking;
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return container.getMealList().size();
	}

	@Override
	public SSRItemModel getItem(int position) {
		// TODO Auto-generated method stub
		SSRItemModel item = container.getMealList().get(position);
		return item;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		if (vi == null){
			holder = new ViewHolder.MealHolder();
			vi = inflater.inflate(R.layout.item_ssr_meal, null);
			holder.img = (ImageView) vi.findViewById(R.id.item_meal_img);
			holder.name = (TextView) vi.findViewById(R.id.item_meal_name);
			holder.dollar = (TextView) vi.findViewById(R.id.item_meal_dollar);
			holder.sens = (TextView) vi.findViewById(R.id.item_meal_sens);
			holder.currency = (TextView) vi.findViewById(R.id.item_meal_currency);
			holder.expandIcon = (ImageView) vi.findViewById(R.id.item_meal_expand);
			holder.bundleLayout = (LinearLayout) vi.findViewById(R.id.item_meal_bundle);
			holder.bundleContainer = (LinearLayout) vi.findViewById(R.id.item_meal_bundle_view);
 			vi.setTag(holder);
		}else{
			holder = (ViewHolder.MealHolder) vi.getTag();
		}
		
		AQuery taq = this.aq.recycle(vi);
		SSRItemModel mSSR = container.getMealList().get(position);
		String url = ConstantHolder.BASE_URL+mSSR.getImage();
		LogHelper.debug("Picture code " + mSSR.getFeeCode()) ;
//		taq.id(holder.img).image(url, false, true);
		ImageLoader.getInstance().displayImage(url, taq.id(holder.img).getImageView());
		taq.id(holder.name).text(mSSR.getDescripion());
		taq.id(holder.currency).text(booking.getCurrencyCode());
		String value = String.valueOf(mSSR.getAmountTotal());
		String[] splitTotal = value.split("\\.");
		taq.id(holder.dollar).text(splitTotal[0]);
		taq.id(holder.sens).text(splitTotal[1]);
		
		LinearLayout bundlerLayout = holder.bundleLayout;
		bundlerLayout.removeAllViews();
		List<SSRItemModel> personMealList = person.getMealItem(container.isDepart(), container.getMealSegPos());
		if (personMealList == null || personMealList.size()<=0){
			LogHelper.debug("No Meal Selected");
			aq.id(holder.bundleContainer).gone();
		}else{
			for (int mealIndex = 0; mealIndex < personMealList.size(); mealIndex++){
				SSRItemModel personMeal = personMealList.get(mealIndex);
				if (container.getMealBundle() != null){
					if (personMeal != null &&
							personMeal.getSsrCode() != null &&
							personMeal.getSsrCode().equals(mSSR.getSsrCode())){
						aq.id(holder.bundleContainer).visible();
						LogHelper.debug("Meal Selected");
						for (int i = 0; i < container.getMealBundle().size();i++){
							SSRItemModel mBundle = container.getMealBundle().get(i);
							View bundleView = inflater.inflate(R.layout.item_meal_bundle, null);
							ImageView img = (ImageView) bundleView.findViewById(R.id.item_bundle_indicator);
							TextView name = (TextView) bundleView.findViewById(R.id.item_bundle_name);
							TextView sen = (TextView) bundleView.findViewById(R.id.item_bundle_sens);
							TextView dollar = (TextView) bundleView.findViewById(R.id.item_bundle_dollar);
							TextView currency = (TextView) bundleView.findViewById(R.id.item_bundle_currency);
							
							name.setText(mBundle.getDescripion());
							currency.setText(booking.getCurrencyCode());
							String bundleCost = String.valueOf(mBundle.getAmountTotal());
							String[] costSplit = bundleCost.split("\\.");
							dollar.setText(costSplit[0]);
							sen.setText(costSplit[1]);
							bundlerLayout.addView(bundleView);
						}
						taq.id(holder.expandIcon).image(R.drawable.tick_3);
						break;
					}else{
						aq.id(holder.bundleContainer).gone();
						taq.id(holder.expandIcon).image(R.drawable.icon_plus);
					}
				}else{
					aq.id(holder.bundleContainer).gone();
				}
			}
		}
		
		return vi;
	}
	
}