package com.airasia.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.airasia.holder.ViewHolder;
import com.airasia.mobile.R;
import com.airasia.model.FareClassUpgrade;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;

public class UpgradeFlightAdapter extends BaseExpandableListAdapter{

	AQuery aq;
	Activity act;
	LayoutInflater inflater;
	FareClassUpgrade upgrade;
	String currency, price;
	public boolean isSelected = false;
	ViewHolder.BookingHeaderHolder holder;
	ViewHolder.TextListHolder childHolder;
	
	public UpgradeFlightAdapter(Activity _act, View _v, FareClassUpgrade _upgrade
						, String _price, String _currency){
		this.upgrade = _upgrade;
		this.act = _act;
		this.inflater = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.aq = new AQuery(_v);
		this.price = _price;
		this.currency = _currency;

		LogHelper.debug("Children Count " + upgrade.getBenefits().size());
	}
	
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return upgrade.getBenefits().get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
		if (v == null){
			childHolder = new ViewHolder.TextListHolder(); 
			v = inflater.inflate(R.layout.item_upgrade_item, null);
			childHolder.text1 = (TextView) v.findViewById(R.id.upgrade_item_description);
			v.setTag(childHolder);
		}else{
			childHolder = (ViewHolder.TextListHolder) v.getTag();
		}
		AQuery taq = this.aq.recycle(v);
		taq.id(childHolder.text1).text(upgrade.getBenefits().get(childPosition));
		
		
		return v;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		LogHelper.debug("Count Child " + upgrade.getBenefits().size());
		return this.upgrade.getBenefits().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
		if (v == null){
			v = inflater.inflate(R.layout.item_upgrade_header, null);
			holder = new ViewHolder.BookingHeaderHolder();

			holder.name = (TextView) v.findViewById(R.id.upgrade_name);
			holder.textV1 = (TextView) v.findViewById(R.id.upgrade_header_dollar);
			holder.textV2 = (TextView) v.findViewById(R.id.upgrade_header_currency);
			holder.textV3 = (TextView) v.findViewById(R.id.upgrade_header_sen);
			holder.img1 = (ImageView) v.findViewById(R.id.upgrade_select_btn);
			
			v.setTag(holder);
		}else{
			holder = (ViewHolder.BookingHeaderHolder) v.getTag();
		}
		
		AQuery taq = this.aq.recycle(v);
		taq.id(holder.name).text(upgrade.getUpgrade_title());
		taq.id(holder.textV2).text(currency);
		
		if (isSelected){
			taq.id(holder.img1).getView().setActivated(false);
			((ExpandableListView) parent).expandGroup(groupPosition);
			
		}else{
			taq.id(holder.img1).getView().setActivated(true);
			((ExpandableListView) parent).collapseGroup(groupPosition);
		}
		
		String[] priceStr = price.trim().split("\\.");
		taq.id(holder.textV1).text(priceStr[0]);
		taq.id(holder.textV3).text(priceStr[1]);
		
		if(((ExpandableListView) parent).isGroupExpanded(groupPosition)){
			LogHelper.debug("Is Expanded Group");
		}
			
		
		return v;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}

}
