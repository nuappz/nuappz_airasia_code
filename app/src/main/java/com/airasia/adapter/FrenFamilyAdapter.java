package com.airasia.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.airasia.holder.ViewHolder;
import com.airasia.holder.ViewHolder.FrenFamilyHolder;
import com.airasia.mobile.R;
import com.airasia.model.MemberInfoModel;
import com.airasia.util.ConstantHelper;
import com.androidquery.AQuery;

public class FrenFamilyAdapter extends BaseExpandableListAdapter{

	AQuery aq;
	List<MemberInfoModel> items;
	Activity act;
	ViewHolder.FrenFamilyHolder childHolder;
	ViewHolder.FrenFamilyHolder parentHolder;
	LayoutInflater inflater;
	public int positionExpand = -1;
	
	public FrenFamilyAdapter(View view, Activity _act, List<MemberInfoModel> _items){
		aq = new AQuery(view);
		this.items = _items;
		this.act = _act;
		inflater = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}
	
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		if (vi == null){
			childHolder = new ViewHolder.FrenFamilyHolder();
			vi = inflater.inflate(R.layout.item_child_fren_family, null);

			childHolder.fName = (TextView) vi.findViewById(R.id.child_fren_firstname);
			childHolder.lName = (TextView) vi.findViewById(R.id.child_fren_lastname);
			childHolder.dob = (TextView) vi.findViewById(R.id.child_fren_dob);
			childHolder.national = (TextView) vi.findViewById(R.id.child_fren_nationality);
			childHolder.passport = (TextView) vi.findViewById(R.id.child_fren_passport);
			childHolder.issue = (TextView) vi.findViewById(R.id.child_fren_issue);
			childHolder.expiration = (TextView) vi.findViewById(R.id.child_fren_expiration);
			vi.setTag(childHolder);
		}else{
			childHolder = (ViewHolder.FrenFamilyHolder) vi.getTag();
		}
		
		AQuery taq = this.aq.recycle(vi);
		MemberInfoModel model = items.get(groupPosition);
		
		taq.id(childHolder.fName).getTextView().setText(model.getfName());
		taq.id(childHolder.lName).getTextView().setText(model.getlName());
		taq.id(childHolder.dob).getTextView().setText(model.getDisplayDate());
		taq.id(childHolder.national).getTextView().setText(model.getNationality());
		taq.id(childHolder.passport).getTextView().setText(""+model.getPassport());
		taq.id(childHolder.issue).getTextView().setText(model.getIssuingCountry());
		taq.id(childHolder.expiration).getTextView().setText(ConstantHelper.getDateFromddMMyyyy(model.getExpiredDate()));
		
		return vi;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		if (vi == null){
			parentHolder = new ViewHolder.FrenFamilyHolder();
			vi = inflater.inflate(R.layout.item_group_fren_family, null);
			parentHolder.fName = (TextView) vi.findViewById(R.id.group_displayName);
			parentHolder.divider = (View) vi.findViewById(R.id.group_divider);
			vi.setTag(parentHolder);
		}else{
			parentHolder = (ViewHolder.FrenFamilyHolder) vi.getTag();
		}
		
		AQuery taq = this.aq.recycle(vi);
		MemberInfoModel model = items.get(groupPosition);
		taq.id(parentHolder.fName).getTextView().setText(model.getfName());
		if(positionExpand == groupPosition){
			taq.id(parentHolder.divider).gone();
		}else{
			taq.id(parentHolder.divider).visible();
		}
		return vi;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public MemberInfoModel getFren(int position){
		return this.items.get(position);
	}
	
}