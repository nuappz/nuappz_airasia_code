package com.airasia.mobile;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;

import com.airasia.adapter.PriceSummaryAdapter;
import com.airasia.model.BookingInfoModel;
import com.androidquery.AQuery;

public class PriceSummaryActivity extends ActionBarActivity {

	Handler handler;
	BookingInfoModel mInfo; 
	AQuery aq;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_view);
		Bundle extra = getIntent().getExtras();
		aq = new AQuery(this);
		
		mInfo = (BookingInfoModel) extra.getSerializable("info");
		
		handler = new Handler();
		handler.postDelayed(runnable, 300);
		 
	}

	
	
	Runnable runnable = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			setupSummaryList();
		}
	};
	
	private void setupSummaryList(){
		if (mInfo != null){
			PriceSummaryAdapter mAdapter = new PriceSummaryAdapter(aq.id(R.id.list_view_container).getListView()
						,PriceSummaryActivity.this, mInfo);	
			aq.id(R.id.list_view_container).getListView().setAdapter(mAdapter);
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if (handler != null){
			handler.removeCallbacks(runnable);
		}
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		if (handler == null){
			handler = new Handler();
			handler.postDelayed(runnable, 300);
		}
		super.onResume();
	}
	
	
}
