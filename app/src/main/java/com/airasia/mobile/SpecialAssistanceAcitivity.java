package com.airasia.mobile;

import com.androidquery.AQuery;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;

public class SpecialAssistanceAcitivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.activity_assistance);
		setupActionBar();
		super.onCreate(savedInstanceState);
	}
	
	private void setupActionBar() {
		
        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowCustomEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        //ab.setIcon(R.drawable.logo_aa);
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.custom_action_bar, null);

        AQuery tempQ = new AQuery(v);
        
        tempQ.id(R.id.aa_logo).gone();
        tempQ.id(R.id.app_title).text("");
        tempQ.id(R.id.leftButton_view).gone();
        tempQ.id(R.id.rightDone).text(R.string.ok_text);
        tempQ.id(R.id.rightButton_done).visible();

        ab.setCustomView(v);
        ab.setDisplayUseLogoEnabled(false);
        ab.setDisplayShowHomeEnabled(false);
    }
	
}
