package com.airasia.mobile;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.airasia.adapter.CustomCalendarAdapter;
import com.airasia.model.CalendarModel;
import com.airasia.util.ConstantHelper;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;

public class CustomCalendarActivity extends Activity {

	public final static String DEPARTURE_DATE ="DEPARTURE_DATE";
	public final static String RETURN_DATE ="RETURN_DATE";
	
	public static int TOTAL_CALENDAR_DAYS = 420;
	
	AQuery aq;
	int departId, returnId;
	CustomCalendarAdapter customAdapter;
	
	List<CalendarModel> calendarList;
	
	ListView calListView;
	
	Date startDate;
	Date endDate;
	
	Date pickedStartDate;
	Date pickedEndDate;
	
	
//	 @Override
//	 public boolean onCreateOptionsMenu(Menu menu) {
//	     // Inflate the menu; this adds items to the action bar if it is present.
//		 super.onCreateOptionsMenu(menu);
//		 
//		 MenuItem item;
//		 
//		 if(pickedEndDate==null) {
//			 item = menu.add (R.string.one_way_trip);
//	        } else {
//	        	item = menu.add (R.string.done);
//	        }
//		 
//		 item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
//		 
//		  item.setOnMenuItemClickListener (new OnMenuItemClickListener(){
//		    @Override
//		    public boolean onMenuItemClick (MenuItem item){
//		    	Bundle bundle = new Bundle();
//		    	if(pickedStartDate!=null)
//		    	{
//		    		bundle.putSerializable(DEPARTURE_DATE, pickedStartDate);
//		    	}
//		    	if(pickedEndDate!=null)
//		    	{
//		    		bundle.putSerializable(RETURN_DATE, pickedEndDate);
//		    	}
//		        Intent intent = new Intent();
//		        intent.putExtras(bundle);
//		        setResult(RESULT_OK, intent);
//		        finish();
//		      return true;
//		    }
//		  });
//		 
//		 return true;
//
//	 }
	
	
	public void donePressed(View view)
	{
		Bundle bundle = new Bundle();
    	if(pickedStartDate!=null)
    	{
    		bundle.putSerializable(DEPARTURE_DATE, pickedStartDate);
    	}
    	if(pickedEndDate!=null)
    	{
    		bundle.putSerializable(RETURN_DATE, pickedEndDate);
    	}
    	bundle.putInt("departId", departId);
    	bundle.putInt("returnId", returnId);

    	Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
	}
	 
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_calendar);
		
		Bundle extras = getIntent().getExtras();
		if (extras !=null ){
			departId = extras.getInt("departId", 0);
			returnId = extras.getInt("returnId", 0);
		}
		
		aq = new AQuery(this);
		calListView = (ListView) findViewById(R.id.calendar_list);
		calendarList = new ArrayList<CalendarModel>();
		
		resetCalendarList();

		Calendar cal = Calendar.getInstance();

	    DateFormatSymbols dfs = new DateFormatSymbols();
	    String weekdays[] = dfs.getShortWeekdays();
	    
	    LogHelper.debug("first day of weeks 1 = "+cal.getFirstDayOfWeek());

	    if(cal.getFirstDayOfWeek()==1)
	    {
	    	aq.id(R.id.weekTitle1).text(weekdays[Calendar.SUNDAY]);
		    aq.id(R.id.weekTitle2).text(weekdays[Calendar.MONDAY]);
		    aq.id(R.id.weekTitle3).text(weekdays[Calendar.TUESDAY]);
		    aq.id(R.id.weekTitle4).text(weekdays[Calendar.WEDNESDAY]);
		    aq.id(R.id.weekTitle5).text(weekdays[Calendar.THURSDAY]);
		    aq.id(R.id.weekTitle6).text(weekdays[Calendar.FRIDAY]);
		    aq.id(R.id.weekTitle7).text(weekdays[Calendar.SATURDAY]);
	    }
	    else
	    {
	    	
		    aq.id(R.id.weekTitle1).text(weekdays[Calendar.MONDAY]);
		    aq.id(R.id.weekTitle2).text(weekdays[Calendar.TUESDAY]);
		    aq.id(R.id.weekTitle3).text(weekdays[Calendar.WEDNESDAY]);
		    aq.id(R.id.weekTitle4).text(weekdays[Calendar.THURSDAY]);
		    aq.id(R.id.weekTitle5).text(weekdays[Calendar.FRIDAY]);
		    aq.id(R.id.weekTitle6).text(weekdays[Calendar.SATURDAY]);
		    aq.id(R.id.weekTitle7).text(weekdays[Calendar.SUNDAY]);
	    }
	    

		// get data from the table by the ListAdapter
		customAdapter = new CustomCalendarAdapter(this, R.layout.item_calendar_row, calendarList);

		calListView.setAdapter(customAdapter);
		
		View footerView = ((LayoutInflater) CustomCalendarActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.empty_footer, null, false);
		calListView.addFooterView(footerView);
		
		updateDateHeader();
	}
	
	private void updateDateHeader()
	{
		if(pickedStartDate!=null)
		{
			Button doneButton = aq.id(R.id.doneButton).getButton();
			if(doneButton.getVisibility()==View.GONE)
			{
				aq.id(R.id.doneButton).animate(R.anim.button_slide_up).visible();
			}
			
			if(pickedEndDate==null)
			{
				aq.id(R.id.doneButton).text((getResources().getString(R.string.one_way_trip)).toUpperCase());
			}
			else
			{
				aq.id(R.id.doneButton).text((getResources().getString(R.string.done)).toUpperCase());
			}
		}
		
		customAdapter.updateAdapter(startDate,endDate,pickedStartDate,pickedEndDate);
		
		aq.id(R.id.departDate).text(ConstantHelper.dateForDayMonthYear(pickedStartDate));
		aq.id(R.id.returnDate).text(ConstantHelper.dateForDayMonthYear(pickedEndDate));
		
		
		
		
		
		
		//this.invalidateOptionsMenu();
	}
	
	
	private void resetCalendarList()
	{
		startDate = new Date();
	    
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		
		startDate = c.getTime();
		
		c.add(Calendar.DATE, TOTAL_CALENDAR_DAYS+1);  // number of days to add
		
		endDate = c.getTime();
		
		LogHelper.debug("start date = "+startDate);
		LogHelper.debug("end date = "+endDate);
	    
		c.add(Calendar.DATE, 31);
	    Date endDateAdd31days = c.getTime();

	    Date dateCounter = new Date();
	    
	    while (dateCounter.before(endDateAdd31days)) {
	    	
	    	
	    	
	    	calendarList.add(new CalendarModel(dateCounter));
	    	
	    	
	    	c = Calendar.getInstance();
			c.setTime(dateCounter);
			c.add(Calendar.MONTH, 1);
			
			dateCounter = c.getTime();
	        
			
	    }
	}
	
	public void calendarCellClicked(View v)
	{
		
		Object obj = v.getTag();
		if (obj instanceof Date) {
			Date date = (Date)obj;
			LogHelper.debug("pressed "+date);
			
			if(pickedStartDate==null)
			{
				pickedStartDate = date;
			}
			else if (pickedEndDate==null) {
	            
	            if (pickedStartDate.before(date) || pickedStartDate.equals(date)) {
	                pickedEndDate = date;
	            }
	            else
	            {
	                pickedStartDate = date;
	            }
	        }
	        else
	        {
	            pickedStartDate = date;
	            pickedEndDate = null;
	        }
			
			updateDateHeader();
		}
		
	}
	
}
