package com.airasia.mobile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.scheme.LayeredSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.params.HttpParams;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.format.Time;
import android.util.Log;

import com.airasia.holder.GcmHolder;
import com.airasia.holder.PrefsHolder;
import com.airasia.util.ConstantHelper;
import com.airasia.util.LogHelper;
import com.google.android.gcm.GCMBaseIntentService;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {
	
	public static final int START_MIDNIGHT = 0 , END_MIDNIGHT = 7;
	
	private static final String TAG = "GCMIntentService";
	public static boolean registered = false;
	static int reminderCounter = 1000;
	public static final Set<String> previousTypeList = new HashSet<String>(
			Arrays.asList(new String[] { "kick_off", "highlights_video" }));

	public GCMIntentService() {
		super(GcmHolder.SENDER_ID);
	}
 
	@Override
	protected void onRegistered(Context context, String registrationId) {
		LogHelper.debug("gcm services registered = "+registrationId);
		// displayMessage(context, getString(R.string.gcm_registered));
		// ServerUtilities.register(context, registrationId);

		if(registrationId!=null && registrationId.length()>0)
		{
			SharedPreferences prefs = context.getSharedPreferences(
					PrefsHolder.PREFS, MODE_PRIVATE);
			SharedPreferences.Editor edit = prefs.edit();
			edit.putString(PrefsHolder.C2DM_REGISTER_ID, registrationId);
			edit.commit();

//			getProfile(context,registrationId);
		}
		

	}

//	private void getProfile(Context c,String registrationId) {
//		SharedPreferences prefs = c.getSharedPreferences(PrefsHolder.PREFS,
//				MODE_PRIVATE);
//		LogHelper.debug("GCM getProfile id "
//				+ prefs.getString(PrefsHolder.UID, "-1")
//				+ " to update push notification "
//				+ registrationId);
//
//		HttpClient httpclient = new DefaultHttpClient();
//		workAroundReverseDnsBugInHoneycombAndEarlier(httpclient);
//		HttpPost httppost = new HttpPost(ConstantHolder.URL);
//
//		try {
//			// Add your data
//			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
//			nameValuePairs.add(new BasicNameValuePair("action", ""
//					+ CustomHttpClient.CONN_GET_PROFILE));
//			nameValuePairs.add(new BasicNameValuePair("mid", prefs.getString(
//					PrefsHolder.UID, "-1")));
//			nameValuePairs.add(new BasicNameValuePair("ver", CustomHttpClient.httpVersion2));
//			nameValuePairs.add(new BasicNameValuePair("lang_id", ConstantHolder.lang_id));
//			nameValuePairs.add(new BasicNameValuePair("device_id", Secure
//					.getString(getContentResolver(), Secure.ANDROID_ID)));
//			nameValuePairs.add(new BasicNameValuePair("operating_system",
//					ConstantHolder.OPERATING_SYSTEM));
//			nameValuePairs.add(new BasicNameValuePair("registration_id", registrationId));
//			nameValuePairs.add(new BasicNameValuePair("brand",
//					android.os.Build.BRAND));
//			nameValuePairs.add(new BasicNameValuePair("model",
//					android.os.Build.MODEL));
//			nameValuePairs.add(new BasicNameValuePair("os_version", ""
//					+ android.os.Build.VERSION.SDK_INT));
//			PackageInfo pInfo;
//			String versionName = "";
//			try {
//				pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//				versionName = pInfo.versionName;
//			} catch (NameNotFoundException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//			nameValuePairs.add(new BasicNameValuePair("app_version",
//					versionName));
//			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//
//			// Execute HTTP Post Request
//			HttpResponse response = httpclient.execute(httppost);
//			HttpEntity entity = response.getEntity();
//			InputStream is = entity.getContent();
//			String result = convertStreamToString(is);
//			LogHelper.debug("Register token = "+result);
//			
//			
//			String error = "";
//			DocumentBuilder builder;
//			try {
//				builder = DocumentBuilderFactory.newInstance()
//						.newDocumentBuilder();
//				Document doc = builder.parse(new InputSource(
//						new StringReader(result)));
//				Element element = doc.getDocumentElement();
//
//				int responseStatus = XmlHelper.getAttributeInteger(
//						element, CustomHttpClient.RESPONSE_ID_TAG);
//				String responseMessage = XmlHelper
//						.getAttributeText(element,
//								CustomHttpClient.RESPONSE_TEXT_TAG);
//
//				
//				// loadRegisteredDevice();
//				if (responseMessage != null
//						&& responseMessage.length() > 0) {
//					if (responseStatus == CustomHttpClient.STATUS_DEVICE_LIMITED) {
//						
//					} else {
//						error = responseMessage;
//					}
//				} else {
//					if (responseStatus == CustomHttpClient.STATUS_SUCCESS) {
//						
//						error = "SUCCESS :\n("+registrationId+")";
//						
//						
//					} else {
//						// Fail
//						error = "Failed : "+responseStatus;
//
//					}
//				}
//
//			} catch (ParserConfigurationException e) {
//				e.printStackTrace();
//				error = getString(
//						R.string.error_connection_default,
//						e.toString());
//			} catch (IOException e) {
//				e.printStackTrace();
//				error = getString(
//						R.string.error_connection_default,
//						e.toString());
//			} catch (SAXException e) {
//				e.printStackTrace();
//				error = getString(
//						R.string.error_connection_default,
//						e.toString());
//			} catch (Exception e) {
//				e.printStackTrace();
////				error = getString(
////						R.string.error_connection_default,
////						e.toString());
//			} finally {
//				if (error != null && error.length() > 0) {
//					// Error message alert box
//					prefs.edit().putString("error_gcm", "Update token result : "+error).commit();
//				} 
//			}
//			
//			
//
//		} catch (ClientProtocolException e) {
//			// TODO Auto-generated catch block
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//		}
//		/*
//		 * RequestParams params = new RequestParams(); params.put("mid",
//		 * prefs.getString(PrefsHolder.UID, "-1")); params.put("device_id",
//		 * Secure.getString(getContentResolver(), Secure.ANDROID_ID));
//		 * params.put("operating_system", CH.OPERATING_SYSTEM);
//		 * params.put("registration_id",
//		 * prefs.getString(PrefsHolder.C2DM_REGISTER_ID, ""));
//		 * params.put("brand", android.os.Build.BRAND); params.put("model",
//		 * android.os.Build.MODEL);
//		 * 
//		 * CustomHttpClient.post(this, CustomHttpClient.CONN_GET_PROFILE,
//		 * params, new AsyncHttpResponseHandler() {
//		 * 
//		 * @Override public void onStart() { }
//		 * 
//		 * @Override public void onSuccess(String response) {
//		 * Logger.debug(response); String error = ""; DocumentBuilder builder;
//		 * try { builder = DocumentBuilderFactory.newInstance()
//		 * .newDocumentBuilder(); Document doc = builder.parse(new InputSource(
//		 * new StringReader(response))); Element element =
//		 * doc.getDocumentElement();
//		 * 
//		 * int responseStatus = ReadContent .getAttributeInteger(element,
//		 * CustomHttpClient.RESPONSE_ID_TAG); String responseMessage =
//		 * ReadContent .getAttributeText(element,
//		 * CustomHttpClient.RESPONSE_TEXT_TAG);
//		 * 
//		 * Logger.debug("GCM get profile response status = " + responseStatus);
//		 * 
//		 * if (responseMessage != null && responseMessage.length() > 0) {
//		 * 
//		 * } else { if (responseStatus == CustomHttpClient.STATUS_SUCCESS) {
//		 * 
//		 * } else { // Fail error = getString(
//		 * R.string.error_connection_default, error);
//		 * 
//		 * } }
//		 * 
//		 * } catch (ParserConfigurationException e) { e.printStackTrace(); error
//		 * = getString( R.string.error_connection_default, e.toString()); }
//		 * catch (IOException e) { e.printStackTrace(); error = getString(
//		 * R.string.error_connection_default, e.toString()); } catch
//		 * (SAXException e) { e.printStackTrace(); error = getString(
//		 * R.string.error_connection_default, e.toString()); } catch (Exception
//		 * e) { e.printStackTrace(); error = getString(
//		 * R.string.error_connection_default, e.toString()); } finally { if
//		 * (error != null && error.length() > 0) {
//		 * 
//		 * } } }
//		 * 
//		 * @Override public void onFailure(Throwable throwable) { }
//		 * 
//		 * });
//		 */
//
//	}

	
	  private static String convertStreamToString(InputStream is) {
	  
	  BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	  StringBuilder sb = new StringBuilder();
	  
	  String line = null; try { while ((line = reader.readLine()) != null) {
	  sb.append((line + "\n")); } } catch (IOException e) {
	  e.printStackTrace(); } finally { try { is.close(); } catch (IOException
	 e) { e.printStackTrace(); } } return sb.toString(); 
	 }
	 

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Log.i(TAG, "Device unregistered");
		/*
		 * displayMessage(context, getString(R.string.gcm_unregistered)); if
		 * (GCMRegistrar.isRegisteredOnServer(context)) {
		 * ServerUtilities.unregister(context, registrationId); } else { // This
		 * callback results from the call to unregister made on //
		 * ServerUtilities when the registration to the server failed.
		 * Log.i(TAG, "Ignoring unregister callback"); }
		 */
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		LogHelper.debug("Push notification received message");
		/*
		 * String message = getString(R.string.gcm_message);
		 * displayMessage(context, message); // notifies user
		 * generateNotification(context, message);
		 */
		generateNotification(context, intent);
	}

	@Override
	protected void onDeletedMessages(Context context, int total) {
		Log.i(TAG, "Received deleted messages notification");
		/*
		 * String message = getString(R.string.gcm_deleted, total);
		 * displayMessage(context, message); // notifies user
		 * generateNotification(context, message);
		 */
	}

	@Override
	public void onError(Context context, String errorId) {
		Log.i(TAG, "Received error: " + errorId);
		
		SharedPreferences prefs = context.getSharedPreferences(PrefsHolder.PREFS, MODE_PRIVATE);
		prefs.edit().putString("error_gcm", "get token failed : "+errorId).commit();
		
		// displayMessage(context, "Failed to register device "+errorId);
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		Log.i(TAG, "Received recoverable error: " + errorId);
		/*
		 * displayMessage(context, getString(R.string.gcm_recoverable_error,
		 * errorId));
		 */
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private static void generateNotification(Context context, Intent intent) {

		/*
		 * // 1 = Markting // 2 = Reminder // 3 = Service (Match Updates) // 4 =
		 * Service (panel)
		 */
		LogHelper.debug("Received push notification = " + intent.getExtras());
		Bundle extra = intent.getExtras();
		if (extra != null) {
			String msg = extra.getString("message");
			String id = extra.getString("id");
			String title = extra.getString("title");
			String collapseKey = extra.getString("collapse_key");
			String nType = extra.getString("ntype");
			String cType = extra.getString("ctype");
			// String from = extra.getString("from");
			String url = extra.getString("url");
			if (nType == null) {
				nType = "";
			}
			if (cType == null) {
				cType = "";
			}
			if (url == null) {
				url = "";
			}
			if (title == null) {
				title = "";
			}
			if (id == null) {
				id = "";
			}
			if (msg == null) {
				msg = "";
			}
			int notificationID;
			try {
				notificationID = Integer.parseInt(collapseKey);
			} catch (NumberFormatException e) {
				reminderCounter++;
				notificationID = reminderCounter;

			}

			int currentapiVersion = android.os.Build.VERSION.SDK_INT;
			TaskStackBuilder stackBuilder;
			Intent resultIntent;
			NotificationCompat.BigTextStyle bigstyle = new NotificationCompat.BigTextStyle().bigText(msg);
			NotificationCompat.Builder mBuilder;
			
			if (currentapiVersion > android.os.Build.VERSION_CODES.KITKAT)
			{
				mBuilder = new NotificationCompat.Builder(
						context).setSmallIcon(0)//notifincation icon
						//.setColor(context.getResources().getColor(0))//Color  my modify
						//.setLargeIcon(drawableToBitmap(context.getResources().getDrawable(R.drawable.ic_not_tablet_large)))
						.setContentTitle(title).setContentText(msg)
						.setDefaults(Notification.DEFAULT_ALL).setStyle(bigstyle)
						.setOnlyAlertOnce(true);
				
				//NotificationCompat.WearableExtender extender =
                       // new NotificationCompat.WearableExtender();
				Bitmap  bm = Bitmap.createBitmap(200, 200, Bitmap.Config.ALPHA_8); 
				bm.eraseColor(android.graphics.Color.RED);
				//in the line above you can change the background image
				//extender.setBackground(bm);
				//mBuilder.extend(extender);
			}
			
//			else if (context.getResources().getBoolean(R.bool.isTabletLand)) {
//				if (currentapiVersion < android.os.Build.VERSION_CODES.JELLY_BEAN)
//				{
//					//24 & 72
//					
//					
//					mBuilder = new NotificationCompat.Builder(
//							context).setSmallIcon(R.drawable.ic_not_tablet_small)
//							.setLargeIcon(drawableToBitmap(context.getResources().getDrawable(R.drawable.ic_not_tablet_large)))
//							.setContentTitle(title).setContentText(msg)
//							.setDefaults(Notification.DEFAULT_ALL).setStyle(bigstyle)
//							.setOnlyAlertOnce(true);
//				}
//				else
//				{
//					//64
//					mBuilder = new NotificationCompat.Builder(
//							context).setSmallIcon(R.drawable.ic_not_tablet_jelly)
//							//.setLargeIcon( drawableToBitmap(context.getResources().getDrawable(R.drawable.ic_not_tablet_large_jelly)))
//							.setContentTitle(title).setContentText(msg)
//							.setDefaults(Notification.DEFAULT_ALL).setStyle(bigstyle)
//							.setOnlyAlertOnce(true);
//				}
//			
//			}
			else
			{
				mBuilder = new NotificationCompat.Builder(
						context).setSmallIcon(R.drawable.ic_launcher)
						.setContentTitle(title).setContentText(msg)
						.setDefaults(Notification.DEFAULT_ALL).setStyle(bigstyle)
						.setOnlyAlertOnce(true);
			}
			
			

//			int drawable = R.drawable.ic_launcher;
//			if (context.getResources().getBoolean(R.bool.isTabletLand)) {
//				drawable = R.drawable.ic_launcher;
//			}
//			
//			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
//					context).setSmallIcon(drawable).setContentTitle(title)
//					.setContentText(msg).setDefaults(Notification.DEFAULT_ALL)
//					.setStyle(bigstyle).setOnlyAlertOnce(true);
//			if (context.getResources().getBoolean(R.bool.isTabletLand)) {
//				mBuilder.setLargeIcon(drawableToBitmap(context.getResources()
//						.getDrawable(R.drawable.ic_launcher)));
//			}

			PendingIntent resultPendingIntent = null;
			int type = 0;
			if (nType.equals("kick_off") || nType.equals("goals")) {
				type = 1;
			} else if (nType.equals("more_apps_push")) {
				type = 5;
			} else if (nType.equals("highlights_video")) {
				type = 2;
			} else if (nType.equals("man_utd_push")) {
				type = 3;
			} else if (nType.equals("partner_push")) {
				type = 4;
			}
			else if(nType.equals("video"))
			{
				type = 7;
			}  else if (nType.equals("news")||nType.equals("football_news")||nType.equals("united_today")||nType.equals("press_conferences")||nType.equals("match_reports")) {
				type = 6;
			}
			
			// New added
//			else if (nType.equals("football_news")) {
//				type = 6;
//			} else if (nType.equals("features_classics")) {
//				type = 7;
//			} else if (nType.equals("interview_video")) {
//				type = 8;
//			} else if (nType.equals("free_video")) {
//				type = 9;
//			} else if (nType.equals("wallpaper")) {
//				type = 10;
//			} else if (nType.equals("history")) {
//				type = 11;
//			} else if (nType.equals("setting")) {
//				type = 12;
//			} else if (nType.equals("interview_news")) {
//				type = 13;
//			} else if (nType.equals("call_to_action")) {
//				type = 14;
//			}else if (nType.equals("win_competition")) {
//				type = 15;
//			}
			
				
			
			else
			{
				type = 99;
			}

			if (currentapiVersion <= android.os.Build.VERSION_CODES.GINGERBREAD_MR1) {
				notificationID = 999;
			}
//			if ((nType.equals("man_utd_push"))
//					&& url.length() > 0) {
//				resultIntent = new Intent(Intent.ACTION_VIEW,
//						Uri.parse(ConstantHelper.getProperUrl(url)));
//				PendingIntent pending = PendingIntent.getActivity(context,
//						notificationID, resultIntent,
//						Intent.FLAG_ACTIVITY_NEW_TASK);
//				mBuilder.setContentIntent(pending);
//			} else {
				stackBuilder = TaskStackBuilder.create(context);
				resultIntent = new Intent(context, MainActivity.class);
				resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//				resultIntent.putExtra("notType", type);
//				resultIntent.putExtra("id", id);
//				resultIntent.putExtra("ctype", cType);
//				resultIntent.putExtra("ntype", nType);
//				resultIntent.putExtra("url", url);
//				stackBuilder.addParentStack(MainActivity.class);
				stackBuilder.addNextIntent(resultIntent);

				resultPendingIntent = stackBuilder.getPendingIntent(
						notificationID, PendingIntent.FLAG_UPDATE_CURRENT);

				mBuilder.setContentIntent(resultPendingIntent);
//			}

			NotificationManager mNotificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
			// mId allows you to update the notification later on.
			Notification not = mBuilder.build();
			
			SharedPreferences prefs = context.getSharedPreferences(
					PrefsHolder.PREFS, MODE_PRIVATE);
//			if (prefs.getBoolean(PrefsHolder.QUITE_MODE, false) == PrefsHolder.IS_QUITE_MODE){
//				
//				Time currentTime = new Time();
//				currentTime.setToNow();
//				if(currentTime.hour >= START_MIDNIGHT && currentTime.hour < END_MIDNIGHT){
//					not.defaults = 0;
//				}
//				else
//				{
//					not.defaults = Notification.DEFAULT_ALL;
//				}
//			}else {
//				not.defaults = Notification.DEFAULT_ALL;
//			}
			
			not.flags |= Notification.FLAG_AUTO_CANCEL;
			mNotificationManager.notify(notificationID, not);
		}

	}

	/*
	 * public static void createNotification(Context context, String title,
	 * String message, String matchID, int notificationID) { NotificationManager
	 * notificationManager = (NotificationManager) context
	 * .getSystemService(Context.NOTIFICATION_SERVICE); Notification
	 * notification = new Notification(R.drawable.ic_launcher,
	 * "Message received", System.currentTimeMillis()); // Hide the notification
	 * after its selected notification.flags |= Notification.FLAG_AUTO_CANCEL;
	 * DatabaseHandler db = new DatabaseHandler(context); Match match =
	 * db.getMatchById(matchID); db.close(); Intent intent = new Intent(context,
	 * MatchActivity.class); intent.putExtra("match", match);
	 * intent.putExtra("from_notification", true);
	 * intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
	 * Intent.FLAG_ACTIVITY_NO_HISTORY); PendingIntent pendingIntent =
	 * PendingIntent.getActivity(context, notificationID, intent, 0);
	 * notification.setLatestEventInfo(context, title, message, pendingIntent);
	 * notificationManager.notify(notificationID, notification);
	 * 
	 * }
	 */
	public static final Bitmap drawableToBitmap(Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			return ((BitmapDrawable) drawable).getBitmap();
		}

		Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
				drawable.getIntrinsicHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		drawable.draw(canvas);

		return bitmap;
	}

	private void workAroundReverseDnsBugInHoneycombAndEarlier(HttpClient client) {
		// Android had a bug where HTTPS made reverse DNS lookups (fixed in Ice
		// Cream Sandwich)
		// http://code.google.com/p/android/issues/detail?id=13117
		SocketFactory socketFactory = new LayeredSocketFactory() {
			SSLSocketFactory delegate = SSLSocketFactory.getSocketFactory();

			@Override
			public Socket createSocket() throws IOException {
				return delegate.createSocket();
			}

			@Override
			public Socket connectSocket(Socket sock, String host, int port,
					InetAddress localAddress, int localPort, HttpParams params)
					throws IOException {
				return delegate.connectSocket(sock, host, port, localAddress,
						localPort, params);
			}

			@Override
			public boolean isSecure(Socket sock)
					throws IllegalArgumentException {
				return delegate.isSecure(sock);
			}

			@Override
			public Socket createSocket(Socket socket, String host, int port,
					boolean autoClose) throws IOException {
				injectHostname(socket, host);
				return delegate.createSocket(socket, host, port, autoClose);
			}

			private void injectHostname(Socket socket, String host) {
				try {
					Field field = InetAddress.class
							.getDeclaredField("hostName");
					field.setAccessible(true);
					field.set(socket.getInetAddress(), host);
				} catch (Exception ignored) {
				}
			}
		};
		client.getConnectionManager().getSchemeRegistry()
				.register(new Scheme("https", socketFactory, 443));
	}
}
