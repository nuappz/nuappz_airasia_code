package com.airasia.holder;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ViewHolder {

	public static class HomePagerHolder{
		public ImageView bg_img;
	}
	
	public static class FrenFamilyHolder{
		public TextView fName, lName, dob, national, mobile, email
				, passport, issue, expiration;
		public View divider;
	}
	
	public static class BookingHolder{
		public boolean checkIn;
		public RelativeLayout layout;
		public TextView reminder, userName;
		public ImageView checkedIn;
	}
	
	public static class BookingHeaderHolder{
		public TextView name, textV1, textV2, textV3;
		public ImageView image1, image2;
		public ImageButton imgBtn1;
		public ImageView img1;
	}
	
	public static class TextListHolder{
		public TextView text1;
	}
	
	public static class FlighItemHolder{
		public TextView fFrom, fTo, fTime, fNUm;
		public ImageView image1, image2;
	}
	
	public static class FlightFareHolder{
		public TextView dTime1, dLocation1, rTime1, rLocation1;
		public TextView dTime2, dLocation2, rTime2, rLocation2;
		public TextView estTime1, flightNum1, estTime2, flightNum2;
		public LinearLayout continousFlight, flightContainer;
		public RelativeLayout adultLayout, kidLayout, infantLayout;
//		public View viewDivider;
		public TextView promoView, discountView, limitSeat, bundleView;
		public TextView adultDollar, adultSen, adultCurrency;
		public TextView kidDollar, kidSen, kidCurrency;
		public TextView infantDollar, infantSen, infantCurrency;
		public ImageView flightIcon, flightIcon2;
	}
	
	public static class GuestHolder{
		public LinearLayout contactContainer, contactSelectView
			, genderContainer, birthContainer, bigIdContainer, adaultViewContainer;
		public ImageView isContact, tick;
		public EditText bigId, fName, lName, relation;
		public EditText email, mobile;
		public Button familyBtn;
		public TextView header, emailText;
		public Button dob, nationality, mr, ms, login;
	}
	
	public static class PriceSummaryHolder{
		public ImageView contFlight;
		public TextView departN, returnN, contN;
		public LinearLayout passangerView, taxView
			, addOnView, bigPView, journeyView;
	}
	
	public static class BaggageHolder{
		public TextView departName, returnName;
		public TextView departKg, returnKg, personName;
		public TextView currency, dollar, sens;
		public LinearLayout departJourney, returnJourney, returnContainer;
		public Button departIncrease, departDecrease, returnIncrease, returnDecrease;
	}
	
	public static class MealHolder{
		public TextView name;
		public TextView currency, dollar, sens;
		public ImageView img, expandIcon;
		public LinearLayout bundleLayout, bundleContainer;
	}
	
	
}
