package com.airasia.holder;

public class PrefsHolder {

	public final static String PREFS = "AIRASIAAPP";
	
	public final static String SSO_TICKET = "SSO_TICKET";
	
	public final static String USER_FIRSTNAME = "USER_FNAME";
	public final static String USER_LASTNAME = "USER_LNAME";
	public final static String USER_USERNAME = "USER_NAME";
	public final static String USER_ADDRESS1 = "USER_ADDRESS1";
	public final static String USER_ADDRESS2 = "USER_ADDRESS2";
	public final static String USER_CITY = "USER_CITY";
	public final static String USER_COUNTRYCODE = "USER_COUNTRYCODE";
	public final static String USER_CULTURECODE = "USER_CULTURECODE";
	public final static String USER_GENDER = "USER_GENDER";
	public final static String USER_NATIONALITY = "USER_NATIONALITY";
	public final static String USER_PASSWORD = "USER_PASSWORD";
	public final static String USER_PERSONALEMAIL = "USER_PERSONALEMAIL";
	public final static String USER_POSTCODE = "USER_POSTCODE";
	public final static String USER_STATE = "USER_STATE";
	public final static String USER_TITLE = "USER_TITLE";
	public final static String USER_CID = "USER_CID";
	public final static String USER_PID = "USER_PID";
	public final static String USER_DOB = "USER_DOB";
	public final static String USER_MOBILE = "USER_MOBILE";
	public final static String USER_NOTIFPREF = "USER_NOTIFPREF";
	public final static String USER_ISBIGC = "USER_ISBIGC";
	public final static String USER_BIG_ID = "USER_BIG_ID";
	
	public final static String USER_TICKET = "USER_TICKET";
	public final static String FIRST_TIME = "FIRS_TIME";
	public final static String IS_LOGIN = "IS_LOGIN";
	public final static String MAPPING_DATE = "MAP_DATE";
	
	public final static String DEFAULT_CURRENCY = "MYR";
	public final static String USER_CURRENCY = "currencyPrefer";

	public final static String SESSION_TICKET = "USER_SESSION";
	
	public static final String UID = "U_ID";

	public static final String C2DM_REGISTER_ID = "c2dm_registerid";
	

	public static final String GEO_STATION = "GeoIpStation";
	public static final String GEO_COUNTRY = "GeoIpCountryCode";
	public static final String GEO_CURRENCY = "UserCurrencyCode";
	
	
}
